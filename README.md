# Guestbook

a guestbook where every collaborators, either as individuals or as a one of time micro research group, can sign.

we sign with style (not with content).

the limitation of the 12 `<span>` become a playground of expression.

- you can remix someone else guestbook entries by copy-pasting part of it (encouraged)
- you can write specific of alternating selectors such as `span:nth-of-type(5)` or `span:nth-of-type(odd)`
- you can add (and remove) text through CSS thanks to `:after` and `:before`
- it's not only span there is a `<html>`, `<body>`, `<h1>`, even `<style>` and `<script>`
- the `<span>` can become something else than letters, a whole drawing?
- layout of `grid`, `flex` are possible on the `<h1>` to simulate complex book or website layout
- media queries can transform it in different situations (on the index and in big?)
- it can be printed and print styles can be specified with `@media print`
- custom variable `--name` can be used to tweak the sketches
- interaction are possible with `:hover`, `:focus`, `@keyframe`
- documentation can be written in the pad under `/* comments */`, including hyperlinks as references

## use

install requierement

    pip3 install requierement.txt


launch

    python3 app.py


you can create a new graph by appending some words to the url like

    http://localhost:5000/mygraph


## setup documentation

This setup uses a [Flask application](https://flask.palletsprojects.com/en/2.2.x/), in order to dynamically instance new graphs.

Each graph is associated to a prefixed pad on a specific [Etherpad]() instance.

It uses [Graphviz python module]() to parse the content of the pad in DOT language and render an `svg`, that is then included back in the page, on the press of the DOT button.

In order to use the content of pads as stylesheet, an ajax request is requiered. It fills the `<style>` tags through a `data-pad` attribute, because some browser (?) show a mimetype restriction error (expecting `text/css` instead of `text/plain`).

## todo

* css styling
* multi seed
* svg hyperlink export