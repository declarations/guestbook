from flask import Flask, request, render_template

app = Flask(__name__)

title = 'Declarations guestbook'
etherpads = 'http://pads.osp.kitchen/p/'
prefix = 'declarations-style'
standard = 'global'

saved = ['empty', 
            'doriane','gijs', 'einar', 'natalia', 'simon', 'vinciane', 'ludi', 
            'session-3', 'session-4', 'session-5',
            'ink-narration', 'skies', 'print-shop-pro',
            'daniel', 'camilo', 'sohyeon', 'lara', 'martin', 'ezn', 'karl', 'florence',
            'flower-bouquet', 
            '3x4', '12h', 'reveal']

# ROUTES
# ------------------------------

@app.route("/")
def index():

    pad_global = etherpads + prefix + '-' + standard
    pads = [[a, etherpads + prefix + '-' + a] for a in saved]

    return render_template(
        'index.html',
        title = title,
        pad_global = pad_global,
        pads = pads)

@app.route("/<id>")
def interface(id):

    pad = etherpads + prefix + '-' + id
    pad_global = etherpads + prefix + '-' + standard

    # render templates
    return render_template(
        'page-interface.html',
        id = id,
        pad = pad,
        pad_global = pad_global,
        title = title)

# in order to be sure that anything written in a pad can't influence/break the interface
# we separate the inferface from the raw rendering through another layer of iframes
@app.route("/<id>--get")
def graph(id):

    pad = etherpads + prefix + '-' + id
    pad_global = etherpads + prefix + '-' + standard

    # render templates
    return render_template(
        'page.html',
        id = id,
        pad = pad,
        pad_global = pad_global,
        title = title)

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0', port='80')